# 9/Modulus

## Module Network System

+ Status: Raw
+ Editor: Arewa Olakunle <arewa.olakunle@gmail.com>

## Goals
The module network system aims to create an architecture for 
multiple **gakp** clients to share the same client space. It takes
a look at the following problems:
+ A way to deploy seperate applications
+ A way to load those applications and their dependencies
+ A way to run those applications
+ A way to remove applications after running it.

And whatever solution is used must meet the following requirements:
+ All js applications must render pure views.
+ Small applications should be easy to write.
+ It should be easy to compose a large application from several
  small ones.
+ Messaging between clients should be straight-forward
+ UI consistency is important, i.e. speed, design and UX should be close
  to the same for each application.
+ The system must act as a tool and not a framework. It must and will be controlled by
  the developers using it.

## Language
The key words **MUST**, **MUST NOT**, **REQUIRED**, **SHALL**, **SHALL NOT**, **SHOULD**,
**SHOULD NOT**, **RECOMMENDED**, **MAY**, and **OPTIONAL** in this document are to be interpreted
as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).

## Preliminaries
+ A **module** is a component specific to particular domain in the gakp system. It is
  composed of subcomponents belonging to the same domain.
+ A **switch** an event caused by an a module asking for another module to come up.


## Module Structure
+ A module **MUST** render its views as [virtual-dom]() elements, hence pure views.
+ Matt Etch's virtual-dom and [hyperscript-helpers]() are **RECOMMENDED**.
