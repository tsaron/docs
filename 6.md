# 6/Admissions

## Admissions

+ Status: Raw
+ Editor: Arewa Olakunle <arewa.olakunle@gmail.com>

## Goals
This main goal of this spec is to facilitate getting a patient on a bed. It describes
the processes involved in tracking the transition of an out-patient to an in-patient.



## AdmissionRequest
This is an entity that describes the properties needed to start the transition from an
out-patient to an in-patient
```js
{
    type: 'object',
    properties: {
        patientid: {
            $ref: 'ObjectID',
            description: "The patient's id"
        },
        assignedto: {
            $ref: 'ObjectID',
            description: 'User to take care of the patient'
        },
        bedid: {
            $ref: 'ObjectID',
            description: 'The bed in ward reserved for the patient'
        },
        comments: {
            type: 'string',
            description: "Doctor's comment on this admission"
        }
    },
    required: ['patientid', 'wardid', 'bedid']
}
```


## Admission
This represents the evidence that a patient as been admitted.
```js
{
    $extend: 'MongoEntity',
    properties: {
        patientid: {
            $ref: 'ObjectID',
            description: "The patient's id"
        },
        assignedto: {
            $ref: 'User',
            description: 'User to take care of the patient'
        },
        user: {
            $ref: 'User',
            description: 'The doctor that admitted the patient'
        },
        bed: {
            $ref: 'AdmissionBed',
            description: 'The bed in ward reserved for the patient'
        },
        comments: {
            type: 'string',
            description: "Doctor's comment on this admission"
        }
    }
}
```


## AdmissionBed
It main goal is to represent the bed asset in admission
```js
{
    $extend: 'object',
    properties: {
        assetid: {
            $ref: 'ObjectID',
            description: 'A reference to the asset that this bed represents'
        },
        bedname: {
            type: 'string',
            description: 'Name attached to the bed'
        },
        wardname: {
            type: 'string',
            description: 'The ward where the bed resides'
        }
    }
}
```

### Create admission
```js
    request('POST', '/api/admission', {
        $ref: 'AdmissionRequest'
    })

    response({
        id: 'ObjectID'
    })
```
