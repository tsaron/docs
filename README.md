## Introduction

This is a collection of all specifications related or unrelated to the
gatekeeper software. The only requirement for a spec to exist here is that
the spec follows the system described in [Spec](2.md)

