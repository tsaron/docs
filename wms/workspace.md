# Workspace

A virtual workspace for gatekeeper's clients

|      Key       |                 Types                  |               Description               |
|----------------|----------------------------------------|-----------------------------------------|
| id             | [ObjectID](type.md#objectid)           | ID of the group                         |
| workspacetitle | `string`                               | Title of the workspace                  |
| workspacetype  | `string`                               | The workspace type                      |
| phone          | [Phone](type.md#phone)                 | workspace's phone number                |
| email          | [Email](type.md#email)                 | workspace's email address               |
| address        | `string`                               | workspace's mailing address             |
| primary        | [ContactPerson](type.md#contactperson) | Workspace's 's primary contact person   |
| secondary      | [ContactPerson](type.md#contactperson) | Workspace's 's secondary contact person |
| status         | `string`                               | Workspace's account status              |
| subscription   | [Subscription](type.md#subscription)   | Workspace's subscription details        |


### Create workspace
`POST` over `api/wms/workspaces`
```js
    var request = type('Workspace')

    var response = {
        id: 'ObjectId'
    }
```


### Read workspace
`GET` over `api/wms/workspaces/{wid:ObjectId}`
```js
    var response = var request = type('Workspace')
```


### Update workspace
`PUT` over `api/wms/workspaces/{wid:ObjectId}`
```js
    var request = type('Workspace')

    var response = type('Workspace')
```

### Search workspace
`GET` over `api/wms/workspaces?query={Qeury_String}`
```js
    var response = type('Workspace')
```

### Delete Workspace
`DELETE` over `api/wms/workspaces/{wid:ObjectId}`
```js
    var response = type('Workspace')
```
